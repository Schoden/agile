const express = require("express")
const app = express()
const path = require('path');
app.use(express.json())
const newsRouter = require('./routes/newsRoutes')
const userRoutes = require("./routes/userRoutes")
const viewRouter = require('./routes/viewRoutes')
const cookieParser = require('cookie-parser')
app.use(cookieParser())



app.use("/api/v1/users", userRoutes)

app.use('/api/v1/news', newsRouter)
app.use("/", viewRouter)
// app.use('/users', userRoutes) // can be used as well



app.use(express.static(path.join(__dirname, 'views')))
module.exports = app
// // start the server on port 4001
// const port = 4001
// app.listen(port, () => {
//     console.log(`App running on port ${port} ..`)
//  })
