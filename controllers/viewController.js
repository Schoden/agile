const path = require("path");

// login page
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "login.html"));
};

// sign page
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "signup.html"));
};

// home page
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "dashboard.html"));
};

exports.getProfile = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'myprofilepage.html'))
}
