const express = require('express');
const userController = require('../controllers/userController');
const authController = require('../controllers/authController');

const router = new express.Router();

router.post('/signup', authController.signup)
router.post('/login', authController.login)
router.get('/logout', authController.logout)

router.patch(
    '/updateMyPassword',
    authController.protect,
    authController.updatePassword,
)
router.patch(
    '/updateMe',
    authController.protect,
    userController.uploadUserPhoto,
    userController.updateMe,
)

router.get('/', userController.getAllUsers);
router.post('/', userController.createUser);

router.get('/:id', userController.getUser);
router.patch('/:id', userController.updateUser);
router.delete('/:id', userController.deleteUser);

module.exports = router;
